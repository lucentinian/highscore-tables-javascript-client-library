# README #

This is a library that can be imported in JavaScript projects (web pages, cordova, phonegap, ...) to connect with our Highscore Tables API

### What is this repository for? ###

* Quick summary

This is the source code for the JavaScript client library for our Highscore Tables API service.

* Version

This is the first version

### How do I get set up? ###

* Summary of set up
Add a <script> tag in your web page to access to the library.js file.

* Configuration

Set the table name and key you want to access to.

* Dependencies

This version has dependencies with jQuery

* Database configuration

N/A

* How to run tests

Tests are not yet defined

* Deployment instructions

TO-DO.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
